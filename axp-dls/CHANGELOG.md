# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.0.0] - 2016-10-24
### Added
- Box shadow classes to refelect multiple levels of depth
- New cobranded accent colors
- Responsive buttons
- Fluid utility class to give elements 100% width
- Chevron buttons
- Action-group classes

### Changed
- Updated membership rewards cobrand accent colors
- Removed jQuery as DLS dependency

## [2.0.0] - 2016-09-26
### Added
- Responsive margin and padding classes
- Margin and padding 1/2/3 (10px/20px/30px) classes

### Changed
- Removed default margins on elements such as text, buttons, etc.
- Z-index on dropdowns:98 and modals:201

## [1.0.6] - 2016-09-21
### Added
- General padding classes
- General margin classes
- Stack class for parent containers where all children have margin-bottoms but not the last-child
- General border classes
- Responsive border classes
- Position classes
- Display classes
- Float left and right classes
- Transparent, hidden, no-scroll, sr-only, and speak-individual classes
- Modal classes from axp-base
- Badge-text classes

### Changed
- Card-block was updated to have responsive padding based on breakpoint
- Cards have default box-shadows
- Tooltip z-index:99
