# Design Language System by American Express  


## Table of Contents
- [Introduction](#introduction)
- [Requirements](#requirements)
- [Project Setup](#setup)
- [Building](#building)
- [Deployment](#deployment)
- [Environments](#environments)
- [Additional Resources](#resources)  

## <a name="introduction"></a>Introduction
Design Language System by American Express  

## <a name="requirements"></a>Requirements
#### Development
NodeJS, Gulp  

## <a name="environments"></a>Environments  

## <a name="setup"></a>Project Setup
1. Download and install [Node](https://nodejs.org)
2. Install local Node dependencies `npm install`

#### Development Build
1. Run through the Project Setup to install the required dependencies.
2. From the root directory, run `npm run build` in the command line.  

#### Gulp Serve
To see the dummy index.html page in your browser with any updates:
1. Npm install browser-sync locally.
2. Run 'gulp serve' in the command line.
3. Make changes to the index.html by adding elements and classes or to other files and see automatic updates in the browser.

## Versioning
See Change Log for additions and changes between versions.

## <a name="resources"></a>Additional Resources
[Node](https://nodejs.org)
[Jekyll](https://jekyllrb.com)

#### HipChat Rooms
- DLS - For questions & concerns
- DLS Updates - For notifications on DLS updates
