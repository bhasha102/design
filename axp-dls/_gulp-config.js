//
// Gulp Config
//
// Our core configuration for gulp.

'use strict';

module.exports = (function (config, argv) {
	config.pkg = require('./package.json');
	// Pass command line arguments
	config.argv = argv;

	// Host name
	config.host = argv.host ? argv.host : 'localhost';

	// Server port (e.g. Override with --port=[PORT])
	config.port = argv.port && (argv.port % 1 === 0) ? argv.port : 8000;

	config.preprocess = {
		VERSION: config.pkg.version
	}

	// Directories
	config.dirs = {
		root: __dirname,
		src: 'source',
		dist:'dist'
	};

	// Styles
	config.styles = {
		src: config.dirs.src + '/styles/**/*.scss',
		dest: config.dirs.dist + '/styles'
	};

	// Scripts
	config.scripts = {
		startScriptPath: config.dirs.src + '/scripts/main',
		src: config.dirs.src + '/scripts/**/*.js',
		dest: config.dirs.dist + '/scripts',
		exportName: 'dls'
	};

	// Iconfonts
	config.iconfont = {
		src: config.dirs.src + '/iconfont/**/*.svg',
		scss: {
			template: config.dirs.src + '/styles/_templates/_iconfont-template.scss',
			output: config.dirs.src +'/styles/modules/style',
			filename: '_iconfont',
			fontRelPath: '../iconfont/'
		},
		dest: config.dirs.dist + '/iconfont',
		svg: {
			dest: config.dirs.dist + '/img/svg/icon',
			relPath: '../img/svg/icon'
		},
		colors: {
		  'red': '#B42C01',
		  'green': '#008566'
		},
		name: 'dls-icons'
	};

	config.images = {
		src: config.dirs.src + '/img/**/*.svg',
		dest: config.dirs.dist + '/img'
	};

	// Clean
	config.clean = [
		config.dirs.dist + '/**/*',
		'!.gitkeep'
	];

	// BrowserSync
	config.browserSync = {
		options: {
			logSnippet: false,
			notify: false,
			ghostMode: false,
			server: true,
			port: 3000,
			files: [config.styles.dest, config.scripts.dest]
		},

		instance: argv._[0] != 'docs' ? null : require('browser-sync').create()
	};
  
	config.webpack = {
		minify: false
	};

	// Object dynamically created in gulp release
	config.semver = {}

	return config;

})({}, require('yargs').argv);
