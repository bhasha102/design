//
// Webpack Configuration
//

var webpack = require('webpack');

module.exports = {

	debug: true,
	entry: './source/scripts/main.js',
	output: {
		path: './dist/scripts/',
		filename: 'dls-min.js'
	},
	module: {
		loaders: [
			// Babel loader
			{
				test: /\.jsx?$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
				 	plugins: ['transform-runtime'],
			 		presets: ['es2015']
				}
			},
			//HTML loader (for importing html templates)
			{
				test: /\.(html)$/,
				loader: 'html-loader'
			}
		]
	},
	plugins:  [
		new webpack.optimize.UglifyJsPlugin({
			compress: { warnings: false }
		})
	]
};
