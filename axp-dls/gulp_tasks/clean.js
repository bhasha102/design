//
// Clean
//

'use strict';

module.exports = function(gulp, config, $) {
	gulp.task('clean', function(cb) {
		return require('del')(config.clean);
	});
}
