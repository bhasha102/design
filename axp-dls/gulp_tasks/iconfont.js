//
// Iconfont
//
// Generate the iconfont(s) from the SVGs

'use strict';

module.exports = function(gulp, config, $) {

	gulp.task('iconfont', function(cb) {
		return gulp.src(config.iconfont.src)
			.pipe($.iconfont({
				fontName: config.iconfont.name,
				appendUnicode: true,
				normalize:true,
				fontHeight: 1001,
				formats: ['eot', 'ttf', 'woff', 'svg'],
				timestamp: Math.round(Date.now()/1000)
			}))
			.on('glyphs', function(glyphs, options) {
				var glyphResult = [];

				for (var i= 0, il= glyphs.length; i < il; ++i) {
					glyphs[i].name = glyphs[i].name
						.split('$').join('-dollar')
						.split('%').join('-percent');

					// Rename axp prefix if exists
					if(glyphs[i].name.indexOf('axp-') > -1)
						glyphs[i].name = glyphs[i].name.replace('axp-', 'dls-');

					glyphResult.push({
						name: glyphs[i].name,
						unicode: glyphs[i].unicode[0].charCodeAt(0).toString(16).toUpperCase()
					});
				}

				gulp.src(config.iconfont.scss.template)
					.pipe($.consolidate('lodash', {
						icons: glyphResult,
						options: options,
						relPath: config.iconfont.scss.fontRelPath,
						svgRelPath: config.iconfont.svg.relPath
					}))
					.pipe($.rename({
						basename: config.iconfont.scss.filename
					}))
					.pipe(gulp.dest(config.iconfont.scss.output))
			})
			.pipe(gulp.dest(config.iconfont.dest))
	});

	gulp.task('iconfont:svg', function(cb) {
		var isDocs = config.argv.docs;
		return new Promise((resolve, reject) => {
			gulp.src(config.iconfont.src)
				.pipe($.imagemin({svgoPlugins: [{removeViewBox: false}]}))
				.pipe($.rename(function (path) {
					// Rename axp prefix if exists
					if(path.basename.indexOf('axp-') > -1)
						path.basename = path.basename.replace('axp-', 'dls-');
					// Rename unicode prefix if exists
					if(path.basename.indexOf('uE') > -1)
						path.basename = path.basename.substr(path.basename.indexOf('-')+1);
				}))
			.pipe(gulp.dest(config.iconfont.svg.dest))
			.on('end', (e) => {
				return resolve(true)
			})
			.on('error', (e) => {
				return reject('Error in gulp task iconfont:svg')
			})
		})
		.then((fulfilled) => {
			return Object.keys(config.iconfont.colors)
				.forEach(colorName => {
					let colorValue = config.iconfont.colors[colorName]
					return new Promise((resolve, reject) => {
						gulp.src(config.iconfont.svg.dest + "/*.svg")
							.pipe($.editXml((xml) => {
								let traverseNodes = (svgNodeObj) => {
									Object.keys(svgNodeObj).forEach(key => {
										let node = svgNodeObj[key]
										return node['$'].fill = colorValue;
									})
									return svgNodeObj;
								}

								if (xml.svg.path)
									xml.svg.path = traverseNodes(xml.svg.path)

								if (xml.svg.circle)
									xml.svg.circle = traverseNodes(xml.svg.circle)

								return xml;
							}))
							.pipe(gulp.dest(config.iconfont.svg.dest + "/" + colorName))
							.on('end', (e) => {
								return resolve(true)
							})
							.on('error', (e) => {
								return reject('Error in gulp task iconfont:svg')
							})
					})
				})
		})
	});
}
