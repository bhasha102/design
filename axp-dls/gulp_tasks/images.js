//
// Images
//

'use strict';

module.exports = function(gulp, config, $) {
	gulp.task('images', function(cb) {
		return gulp.src(config.images.src)
			.pipe(gulp.dest(config.images.dest))
	});
}
