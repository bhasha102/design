//
// Serve task
//

'use strict';

module.exports = function(gulp, config, $, browserSync) {

    var browserSync;

    console.log('\n');
    console.log('   American Express');
    console.log('   Design Language System.');
    console.log('\n');

    try {
        browserSync = require('browser-sync').create();
    } catch (e) {
        console.log('======================================================================');
        console.log('   Please install Browser-Sync for testing.');
        console.log('======================================================================');
        console.log('\n');
    }

    gulp.task('serve', ['watch'], function() {
        try {
            browserSync.init(config.browserSync.options);
            gulp.watch('index.html').on('change', browserSync.reload);
        } catch (e) {
            console.log('NEED TO INSTALL BROWSER-SYNC! npm install browser-sync');
        }

    });
};
