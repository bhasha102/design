//
// Watch task
//

'use strict';

module.exports = function(gulp, config, $) {

	gulp.task('watch', function(cb) {

		return require('run-sequence')(
			//['clean'],
			['build'],
			function() {
				gulp.watch(config.iconfont.src, { interval: 1000 }, ['iconfont', 'iconfont:svg']);
				gulp.watch(config.images.src,   { interval: 1100 }, ['images']);
				gulp.watch(config.styles.src,   { interval: 1200 }, ['styles']);
				gulp.watch(config.scripts.src,  { interval: 1300 }, ['webpack', 'webpack:min']);
				return cb();
			}
		);
	});

	gulp.task('default', ['build']);
};
