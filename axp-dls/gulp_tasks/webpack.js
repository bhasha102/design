//
// Webpack
// The webpack tasks handle webpack output, which includes the core
// scripts output
//

'use strict';

var config = require('../_gulp-config'),
	webpack = require('webpack')

module.exports = (gulp, $) => {

	//
	// The webpack task
	//
	gulp.task('webpack', (done) => {

		var webpackConfig = require('../_webpack-config');
		webpack(webpackConfig, (err, stats) => {
			if(err) console.log(err);
			done();
		});
	});

	gulp.task('webpack:min', (done) => {

		var webpackConfig = require('../_webpack-config-min');
		webpack(webpackConfig, (err, stats) => {
			if(err) console.log(err);
			done();
		});
	});
};
