//
// DLS
//
// Create the main application and pass across our
// base config / options

import DLSApp from './modules/app';

(() => {
	if(!window.jQuery)
		console.warn('DLS: The DLS requires jQuery, please include it before continuing.');

	window.DLS = new DLSApp({
		scope:$('body'),
		modules: {}
	});

	DLS.start();

})();
