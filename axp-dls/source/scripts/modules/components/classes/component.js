//
// Component Base Class
//
// The component base class unifies all the different components with a standard
// set of utility functions that are consistent across each component.
//

import Module from '../../core/module';

export default class Component extends Module {

	constructor (opts) {
		super(opts);
		this.listenCore();
	}

	//
	// Throw standard error messages
	//
	throwError(message) {
		let name = 'Component';

		if(this.component.Name)
			name = this.component.Name;

		console.warn(`[DLS > ${name} Error]: ${message}`);
		return this;
	}

	//
	// Core listen that is applied to the component automatically
	//
	listenCore() {
		this.on('render', (...args) => {
			this.render();
		});
	}

	//
	// Default onClick listen
	//
	onClickListen() {
		if(!this.component)
			return;

		this.scope
			.on(`click${this.component.EventKey}`,
				this.component.Selector, (e) => {
				e.preventDefault();
				e.stopPropagation();
				return this.create({
					target: e.currentTarget,
					config: {
						invoke: true
					}
				});
			});
	}

	//
	// Bind a custom data event listener that can trigger a
	// non-standard interaction event (usually utilised for specific
	// command within a component).
	onDataEventListen() {
		if(!this.component)
			return;

		this.scope
			.on(`data`, this.component.Selector, (e, opts) => {
				e.preventDefault();

				return this.create({
					target: e.currentTarget,
					config: Object.assign({
						invoke: true,
						invokeData: true
					}, opts)
				})
			})
	}

	//
	// Default onHover listen
	//
	onHoverListen() {
		if(!this.component)
			return;

		this.scope
			.on(`${this.platform.event.enter}${this.component.EventKey}`,
				this.component.Selector, (e) => {

				return this.create({
					target: e.currentTarget,
					config: {
						invoke: true,
						invokeHover: true,
						invokeEnter: true
					}
				})
			})
			.on(`${this.platform.event.leave}${this.component.EventKey}`,
				this.component.Selector, (e) => {

				return this.create({
					target: e.currentTarget,
					config: {
						invoke: true,
						invokeHover: true,
						invokeEnter: false
					}
				})
			});
	}

	//
	// Default onFocus listen
	//
	onFocusListen() {
		if(!this.component)
			return;

		this.scope
			.on(`focusin${this.component.EventKey}`,
				this.component.Selector, (e) => {
				e.preventDefault();

				return this.create({
					target: e.currentTarget,
					config: {
						invoke: true,
						invokeFocus: true,
						invokeEnter: true
					}
				})
			})
			.on(`focusout${this.component.EventKey}`,
				this.component.Selector, (e) => {
				e.preventDefault();

				return this.create({
					target: e.currentTarget,
					config: {
						invoke: true,
						invokeFocus: true,
						invokeEnter: false
					}
				})
			});
	}

	//
	// Default Render
	//
	render() {
		if(!this.component)
			return;

		this.scope
			.find(this.component.Selector)
			.each((idx, el) => {
				this.create({
					target:el,
					config: {}
				})
			});
	}

	//
	// Default `Create`
	//
	create(options) {
		if(!this.component)
			return;

		if(!options.target) {
			this.throwError('A `target` must be specified!');
			return this;
		}

		let elements = [];
		options.config = Object.assign({}, options.config);
		options.target = $(options.target);
		options.target.each((idx, el) => {
			let element = this.component._interface.call($(el), options.config);

			if(element)
				elements.push(element);
		});

		return this.created(elements);
	}

	//
	// Return the created elements in flexible format
	//
	created(elements) {
		if(elements.length === 1)
			return elements[0];
		else if(elements.length)
			return elements;
		else
			return null;
	}
}
