//
// Components
// Manages the creation of DLS components
//

import Obj from '../core/obj';
import Module from '../core/module';
import Accordion from './views/accordion';
import Collapse from './views/collapsible';
import Dismiss from './views/dismissible';
import Dropdown from './views/dropdown';
import Modal from './views/modal';
import Pagination from './views/pagination';
import Search from './views/search';
import Select from './views/select';
import Slider from './views/slider';
import Stepper from './views/stepper';
import Tabs from './views/tabs';
import Tooltip from './views/tooltip';
import SmartField from './views/smartfield';
import Carousel from './views/carousel';

export default class Components extends Module {

	constructor (opts) {
		super(opts);
	}

	//
	// Initializer
	//
	initialize() {
		this.registerSubmodules();
		this.listen();
	}

	//
	// Register Submodules
	//
	registerSubmodules() {
		this.set('submodules', {
			accordion: Accordion,
			collapse: Collapse,
			dismiss: Dismiss,
			dropdown: Dropdown,
			modal: Modal,
			pagination: Pagination,
			search: Search,
			select: Select,
			slider: Slider,
			stepper: Stepper,
			tabs: Tabs,
			tooltip: Tooltip,
			smartfield: SmartField,
			carousel: Carousel
		});
	};

	//
	// Bind the listeners
	//
	listen() {
		this.app
			.on('render', (...args) => {
				this.render(args);
			});
	}

	// Render the components asynchronously
	// (generally would not be used but may be used in a web-app situation
	// where components may be added dynamically).
	render() {
		let submodules = this.get('submodules');

		for(let key in submodules) {
			submodules[key].trigger('render');
		}
	}
}
