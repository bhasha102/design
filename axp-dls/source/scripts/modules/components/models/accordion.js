//
// Accordion Model
//

import Model from '../../core/model';

export default class AccordionModel extends Model {}

AccordionModel.prototype.defaults = {
	duration: 280,
	siblings: false,
	closable: true,
	instant: false,
	openAutoExpand: true,
	closeSiblings: true
};
