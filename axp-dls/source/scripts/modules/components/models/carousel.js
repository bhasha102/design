//
// Carousel Model
//

import Model from '../../core/model';

export default class CarouselModel extends Model {}

CarouselModel.prototype.defaults = {
	autoplay: true,
	interval: 5000,
	keyboard: true,
	paused: false,
	slide: 1,
	previous: -1,
	total: 0,
	staggerStart: 0.2,
	staggerIncrement: 0.1
};
