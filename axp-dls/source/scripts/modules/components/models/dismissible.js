//
// Dismissible Model
//

import Model from '../../core/model';

export default class DismissibleModel extends Model {}

DismissibleModel.prototype.defaults = {
	closed: false
};
