//
// Dropdown Model
//

import Model from '../../core/model';

export default class DropdownModel extends Model {}

DropdownModel.prototype.defaults = {
	container: 'dropdown',
	menu: 'dropdown-menu',
	autoclose: true
};
