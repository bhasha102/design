//
// Pagination Model
//

import Model from '../../core/model';

export default class PaginationModel extends Model {}

PaginationModel.prototype.defaults = {
	// The current active page
	current: 1,
	// Default total
	total: 1,
	// The alignment of the pagination
	align: 'center',
	// The threshold at which centering no longer occurs (maximizing space)
	centerThreshold: 500,
	// The breakpoint that the pager controls drop onto the next line
	breakpoint: 500,
	// The offset from the center left when using `align: 'center'`
	centerOffsetLeft: 1,
	// The offset from the center right when using `align: 'center'`
	centerOffsetRight: 1,
	// The duration of the sliding animation when switching pages
	animationDuration: 0.4,
	// Key animation time (when pressing keyboard left/right)
	keyAnimationDuration: 0.1,
	// Next/Previous animation time (time to animate when using the next/previous buttons)
	nextPrevAnimationDuration: 0.35,
	// The duration page numbers should fade in/out as they leave the boundary
	fadeAnimationDuration: 0.3,
	// The base size of a single page item
	itemSize: 26,
	// The ideal item spacing
	itemSpacer: 26,
	// Enable keyboard controls
	keyboard: true
};
