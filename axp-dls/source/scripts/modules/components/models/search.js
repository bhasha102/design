//
// Search Model
//

import Model from '../../core/model';

export default class SearchModel extends Model {}

SearchModel.prototype.defaults = {
	live: true,
	throttle: 200,
	keyevent: 'keydown',
	value: '',
	state: 'default',
	tooltip: true,
	tooltipDelay: 0,
	tooltipDefault: '',
	tooltipSearching: '',
	tooltipActive: 'Clear Search',
	tooltipTheme: 'dark',
	tooltipClasses: '',
	tooltipStyle: '',
	tooltipPlacement: 'top',
	tooltipOffset: '0 0'
};
