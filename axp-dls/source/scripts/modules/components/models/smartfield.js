//
// SmartField Model
//

import Model from '../../core/model';

export default class SmartFieldModel extends Model {}

SmartFieldModel.prototype.defaults = {
	legend: '',
	forceFocus: false
};
