//
// Tabs Model
//

import Model from '../../core/model';

export default class TabsModel extends Model {}

TabsModel.prototype.defaults = {
	closed: false
};
