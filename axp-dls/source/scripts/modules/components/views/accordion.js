//
// Accordion Component
//

import View from '../../core/view';
import Component from '../classes/component';
import AccordionModel from '../models/accordion.js';

//
// Configuration
//
const Name = 'Accordion';
const Selector = '[data-toggle="accordion"]:not([data-ignore])';
const DataKey = 'dls.accordion';
const EventKey = `.${DataKey}`;

//
// Initiator
//
export default class Accordion extends Component {

	constructor (opts) {
		super(opts);
		this.component = AccordionView;

		this.onClickListen();
		this.onDataEventListen();
	}
}

//
// Accordion View
//
class AccordionView extends View {

	constructor (opts) {
		super(opts);

		if(!this.container.is('.open')) {
			this.el.hide();
		}
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {return Selector;}
	static get EventKey() {return EventKey;}

	//
	// Toggle
	//
	toggle(opts) {
		opts = opts || {};

		if(this.target.is('.disabled'))
			return;

		this.emit('toggled');

		if(opts.expand && !this.container.is('.open'))
			this.expand(opts);
		else if(opts.expand === false)
			this.collapse(opts);
		else if(!this.container.is('.open')) {
			this.expand(opts);
		} else if(this.model.get('closable')) {
			this.collapse(opts);
		}
	}

	//
	// Expand
	//
	expand(opts) {
		opts = Object.assign({}, this.model.attributes, opts);

		if(this.transitioning)
			return;

		let {openAutoExpand, closeSiblings, siblings} = this.model.attributes;

		if(opts.openAutoExpanded != null)
			openAutoExpand = opts.openAutoExpanded;

		this.emit('expanding');

		this.target.attr('aria-expanded', 'true');
		this.transitioning = true;
		this.container.addClass('open')
		this.target.attr('aria-expanded', 'true');

		if(openAutoExpand)
			this.openAutoChildren();

		this.el.slideDown(opts.instant ? 0 : opts.duration, () => {
			this.transitioning = false;
			this.emit('expanded');
		});

		if(closeSiblings && this.el.closest('.nav').length && !this.siblings)
			this.closeSiblings();
	}

	//
	// Collapse
	//
	collapse(opts) {
		opts = Object.assign({}, this.model.attributes, opts);

		this.transitioning = true;

		this.emit('collapsing');
		this.el.slideUp(opts.instant ? 0 : opts.duration, () => {
			this.target.attr('aria-expanded', 'false');
			this.container.removeClass('open');
			this.transitioning = false;
			$(window).trigger('resize');
			this.emit('collapsed');
		});

		this.closeChildren();
	}

	//
	// Open child accordions that have been flagged to auto open
	//
	openAutoChildren() {
		this.el
			.find('.accordion:not(.open) [data-auto-expand="true"]')
			.trigger('data', {expand: true, instant: true});
	}

	//
	// Close sibling elements when prompted
	//
	closeSiblings() {
		this.container.siblings('.accordion.open').each((key, el) => {
			$(`> ${Selector}`, el).trigger('data', {expand: false});
		});
	}

	//
	// Close children
	//
	closeChildren() {
		this.el
			.find(`.accordion.open > ${Selector}`)
			.trigger('data', {expand: false});
	}

	//
	// Interface
	//
	static _interface(opts) {
		let $el = this,
			data = $el.data(DataKey),
			$accordionTarget = $el.siblings('.accordion-content'),
			$container = $el.closest('.accordion'),
			cmd = opts.invoke ? 'toggle' : $el.data(),
			config = Object.assign(
				{},
				$el.data(),
				typeof opts === 'object' && opts
			);

		if(!data) {
			data = new AccordionView({
				target: $el,
				el: $accordionTarget,
				container: $container,
				model: new AccordionModel(config)
			});

			$el.data(DataKey, data);
		}

		if(typeof cmd === 'string')
			if(data[cmd] !== undefined) {
				data[cmd](opts);
			}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.el, DataKey);
	}
}
