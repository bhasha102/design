//
// Carousel Component
//

import View from '../../core/view';
import Component from '../classes/component';
import CarouselModel from '../models/carousel.js';
import IndicatorTemplate from './templates/tmpl-carousel-indicator.html';

//
// Configuration
//
const Name = 'Carousel';
const DataKey = 'dls.carousel';
const EventKey = `.${DataKey}`;
//
// Initiator
//
export default class Carousel extends Component {

	constructor (opts) {
		super(opts);
		this.component = CarouselView;
	}
}

//
// Carousel View
//
class CarouselView extends View {

	constructor (opts) {
		super(opts);

		this.setProps();
		this.render();
		this.prepareSlideAnimation();
		this.listen();
		this.refresh();
		this.activateSlide(this.model.get('slide'), true);
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get EventKey() {return EventKey;}

	//
	// Set core props
	//
	setProps() {
		this.sel = {
			slides: this.target.find('.carousel-inner'),
			slide: this.target.find('.carousel-item'),
			controls: this.target.find('.carousel-controls'),
			indicators: this.target.find('.carousel-indicators')
		};

		if(!this.sel.indicators.length)
			this.sel.indicators = $('<div/>');

		this.target
			.attr('tabindex', 0);
	}

	//
	// Render
	//
	render() {
		let indicators = '',
			current = this.model.get('slide'),
			items = this.target
				.find('.carousel-inner > .carousel-item');

		items.each( (idx, el) => {
			el = $(el);

			let uid = this.util.request('uid:gen', 'slide-uid-'),
				index = idx + 1;

			el.attr('aria-labelledby', uid)
				.attr('aria-hidden', current !== index)
				.attr('role', 'tabpanel');

			indicators += this.util.request('template:render',
				IndicatorTemplate,
				{
					id: uid,
					index: index,
					selected: current === index
				}
			);
		});

		this.model
			.set('total', items.length);



		this.sel.indicators
			.empty()
			.append($(indicators));

		return this;
	}

	//
	// Prepare slide animation
	//
	prepareSlideAnimation() {
		this.sel.slide.each( (idx, el) => {
			el = $(el);
			this.addStaggerAnimation(el);
		});
	}

	//
	// Stagger animation
	//
	addStaggerAnimation(el) {
		let delay = Number(this.model.get('staggerStart')),
			increment = Number(this.model.get('staggerIncrement')),
			stagger = el.find('.carousel-anim-stagger');

		stagger.each( (idx, el) => {
			$(el).css({animationDelay: `${delay}s`});
			delay += increment;
		});
	}
	//
	// Listen
	//
	listen() {
		let keyboard = this.model.get('keyboard');

		this.sel.indicators
			.on(`click${EventKey}`, '> li > a',
				this.onIndicatorClick.bind(this)
			);

		this.sel.controls
			.on(`click${EventKey}`, '.carousel-control',
				this.onControlClick.bind(this)
			);

		this.model
			.on('change', this.onChange, this);

		if(keyboard)
			this.target.on(`keydown${EventKey}`,
				this.onKeyDown.bind(this)
			);


		//
		// Expose triggers
		//
		this.on('next', this.next, this)
			.on('prev', this.prev, this)
			.on('pause', this.pause, this)
			.on('cycle', this.cycle, this)
			.on('redraw', this.redraw, this);
	}

	//
	// Indicator Click
	//
	onIndicatorClick(e) {
		let el = $(e.currentTarget),
			index = el.data('index');

		this.set('slide', index);
		e.preventDefault();
	}

	//
	// On Control Click
	//
	onControlClick(e) {
		let el = $(e.currentTarget),
			type = el.data('control');

		if(this[type] !== undefined) {
			e.preventDefault();
			return this[type]();
		}
	}

	//
	// On Key Down
	//
	onKeyDown(e) {
		e.preventDefault();
		if(/input|textarea/i.test(e.target.tagName))
			return;

		switch(e.which) {
			case 9: this.next(); break;
			case 37: this.prev(); break;
			case 39: this.next(); break;
			default: return;
		}
	}

	//
	// Model changed
	//
	onChange(e) {
		if(e.slide != null) {
			this.activateSlide(e.slide);
		}

		if(e.interval != null) {
			this.cycle();
		}
	}

	//
	// Next Slide
	//
	next() {
		let {slide, total} = this.model.attributes;

		slide++;

		if(slide > total)
			slide = 1;

		this.model.set('direction', 'next');
		this.model.set('slide', slide);
		return this;
	}

	//
	// Previous Slide
	//
	prev() {
		let {slide, total} = this.model.attributes;

		slide--;

		if(slide < 1)
			slide = total;

		this.model.set('direction', 'prev');
		this.model.set('slide', slide);
		return this;
	}

	//
	// Set the slide to the specified index
	//
	activateSlide(index, instant) {

		if(index > this.model.get('total')) {
			this.model.attributes.slide = 1;
			index = 1;
		}

		let current = index - 1,
			slideID = this.sel.indicators
				.find(`> li > a:eq(${current})`)
				.attr('aria-controls'),
			slide = this.sel.slides
				.find(`[aria-labelledby="${slideID}"]`);

		this.show(slide, instant);
		this.refresh();

		return this;
	}

	//
	// Show the selected slide
	//
	show(el, instant) {
		let {slide, previous, direction} = this.model.attributes,
			offset = 0,
			others = this.sel.slide
				.not(el[0]),
			isNext = (direction === 'next' ||
				slide > previous && direction == null
			),
			isPrev = (direction === 'prev' ||
				slide < previous  && direction == null
			);

		if(direction !== null) {
			this.model.set('direction', null);
		}

		offset = isNext ? 60 : isPrev ? -60 : 0;

		el.css({
			transform: instant ? '' : `translateX(${offset}px)`,
			opacity: instant ? 1 : 0
		});

		this.sel.slides
			.append(el);


		setTimeout(() => {
			others.css({
				opacity: 0
			});

			el.css({
					transform: `translateX(0px)`,
					opacity: 1
				});
		}, 100);

		el.addClass('in')
			.addClass('carousel-slide-active')
			.attr('aria-hidden', false)

		others
			.removeClass('carousel-slide-active')
			.removeClass('in')
			.attr('aria-hidden', true);

		this.model.set('previous', this.model.get('slide'));
		this.cycle();
	}

	//
	// Pause
	//
	pause() {
		this.model.set('paused', true);
		clearTimeout(this.timeout);

		return this;
	}

	//
	// Play
	//
	play() {
		this.model.set('autoplay', true);
		this.model.set('paused', false);
		this.cycle();

		return this;
	}

	//
	// Cycle
	//
	cycle() {
		let {
			autoplay,
			paused,
			interval
		} = this.model.attributes;

		if(!autoplay || paused)
			return;

		clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
			this.next();
		}, interval);

		return this;
	}

	//
	// Refresh
	//
	refresh() {
		let current = this.model.get('slide') - 1;

		this.sel.indicators
			.find(`> li > a:eq(${current})`)
			.addClass('active')
			.attr('aria-selected', true);

		this.sel.indicators
			.find(`> li > a:not(:eq(${current}))`)
			.removeClass('active')
			.attr('aria-selected', false);
	}

	//
	// Redraw the UI elements of the carousel (such as the indicators),
	// May be used in the circumstance that an item is added to the carousel.
	//
	redraw() {
		return this.render();
	}

	//
	// Interface
	//
	static _interface(config) {
		let $el = $(this),
			data = $el.data(DataKey);

		config = Object.assign($el.data(), config);

		if(!data) {
			data = new CarouselView({
				target: $el,
				model: new CarouselModel(config)
			});

			$el.data(DataKey, data);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
