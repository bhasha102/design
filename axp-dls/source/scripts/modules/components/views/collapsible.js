//
// Collapsible Component
//

import View from '../../core/view';
import Component from '../classes/component';
import CollapsibleModel from '../models/collapsible.js';

//
// Configuration
//
const Name = 'Collapsible';
const Selector = '[data-toggle="collapse"]:not([data-ignore])';
const DataKey = 'dls.collapsible';
const EventKey = `.${DataKey}`;

//
// Initiator
//
export default class Collapse extends Component {

	constructor (opts) {
		super(opts);
		this.component = CollapsibleView;

		this.onClickListen();
	}
}

//
// Collapsible View
//
class CollapsibleView extends View {

	constructor (opts) {
		super(opts);

		if(!this.el.is('.in')) {
			this.el.hide();
		}
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {return Selector;}
	static get EventKey() {return EventKey;}

	//
	// Toggle
	//
	toggle() {
		if(this.target.is('.disabled'))
			return;

		this.emit('toggled');

		if(!this.el.is('.in')) {
			this.expand();
		} else if(this.model.get('closable')) {
			this.collapse();
		}
	}

	//
	// Expand
	//
	expand(opts) {
		opts = Object.assign({}, this.model.attributes, opts);

		if(this.transitioning)
			return;

		this.emit('expanding');

		this.target.attr('aria-expanded', 'true');
		this.transitioning = true;
		this.el
			.addClass('in')
			.attr('aria-expanded', 'true');

		this.el.slideDown(opts.instant ? 0 : opts.duration, () => {
			this.transitioning = false;
			$(window).trigger('resize');
			this.emit('expanded');
		});
	}

	//
	// Collapse
	//
	collapse(e, opts) {
		opts = Object.assign({}, this.model.attributes, opts);

		if(this.transitioning)
			return;

		this.emit('collapsing');

		this.transitioning = true;

		this.el.slideUp(opts.instant ? 0 : opts.duration, () => {
			this.target.attr('aria-expanded', 'false');
			this.el
				.removeClass('in')
				.attr('aria-expanded', 'false');

			this.transitioning = false;
			$(window).trigger('resize');
			this.emit('collapsed');
		});
	}


	//
	// Interface
	//
	static _interface(opts) {
		let $el = this,
			data = $el.data(DataKey),
			collapseTarget = $el.attr('href'),
			$collapseTarget = $(`${collapseTarget}:first`),
			cmd = opts.invoke ? 'toggle' : $el.data(),
			config = Object.assign(
				{},
				$el.data(),
				typeof opts === 'object' && opts
			);

		if(!data) {
			data = new CollapsibleView({
				target: $el,
				el: $collapseTarget,
				model: new CollapsibleModel(config)
			});

			$el.data(DataKey, data);
		}

		if(typeof cmd === 'string')
			if(data[cmd] !== undefined) {
				data[cmd]();
			}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.el, DataKey);
	}
}
