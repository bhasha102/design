//
// Dismissible Component
//

import View from '../../core/view';
import Component from '../classes/component';
import DismissibleModel from '../models/dismissible.js';

//
// Configuration
//
const Name = 'Dismissible';
const Selector = '[data-dismiss]:not([data-ignore])';
const DataKey = 'dls.dismissible';
const EventKey = `.${DataKey}`;

//
// Initiator
//
export default class Dismiss extends Component {

	constructor (opts) {
		super(opts);
		this.component = DismissibleView;

		this.render();
		this.onClickListen();
	}
}

//
// Dismissable View
//
class DismissibleView extends View {

	constructor (opts) {
		super(opts);
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {return Selector;}
	static get EventKey() {return EventKey;}

	//
	// Close
	//
	close() {
		if(this.model.get('closed'))
			return;

		this.emit('closing');

		this.target
			.addClass('anim-fade')
			.removeClass('in');

		this.model.set('closed', true);

		setTimeout(() => {
			this.target.remove();
			this.target.trigger('closed', this);
			this.emit('closed', this);
			this.destroy();
		},300);
	}


	//
	// Interface
	//
	static _interface(opts) {
		let $el = this,
			data = $el.data(DataKey),
			dismissTarget = $el.data('dismiss-target'),
			$dismissTarget,
			cmd = opts.invoke ? 'close' : $el.data(),
			config = Object.assign(
				{},
				$el.data(),
				typeof opts === 'object' && opts
			);

		if(!data) {
			if(dismissTarget)
				$dismissTarget = $(dismissTarget);
			else
				$dismissTarget = $el.closest(`.${config.dismiss}`);

			data = new DismissibleView({
				target: $dismissTarget,
				el: $el,
				model: new DismissibleModel(config)
			});

			$el.data(DataKey, data);
		}else {
			// Component instance already exists
		}

		if(typeof cmd === 'string')
			if(data[cmd] !== undefined) {
				data[cmd]();
			}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
