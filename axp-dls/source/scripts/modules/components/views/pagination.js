//
// Pagination Component
//

import Model from '../../core/model';
import View from '../../core/view';
import Tween from '../../core/tween';
import Component from '../classes/component';
import PaginationModel from '../models/pagination.js';
import Template from './templates/tmpl-pagination.html';
import TemplateItem from './templates/tmpl-pagination-item.html';

//
// Configuration
//
const Name = 'Pagination';
const DataKey = 'dls.pagination';
const EventKey = `.${DataKey}`;

// Clamp utility
var clamp = (min, max, value) => {
	return Math.max(min, Math.min(max, value));
};

//
// Initiator
//
export default class Pagination extends Component {

	constructor (opts) {
		super(opts);
		this.component = PaginationView;
	}

}



//
// Pagination View
//
class PaginationView extends View {

	constructor (opts) {
		super(opts);
		this.prop = {
			suppressChanges: false,
			activeValue: 1,
			breakpoint: false,
			active: false,
			block: {
				pos: 0,
				width: 0,
				activeWidth: 0
			},
			ellipsis: {
				'start': {
					el: null,
					shown: false,
				},
				'end': {
					el: null,
					shown: false,
				}
			}
		};

		this.params = {};

		this.setup();
		this.render();
		this.setProps();
		this.createPages();
		this.listen();

		this.setCurrent();
		this.prop.activeValue = this.model.get('current');
		this.setInitial();
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get EventKey() {return EventKey;}

	//
	// Set the properties
	//
	setProps() {
		this.sel = {
			list: this.target.find('.pagination-list'),
			pager: this.target.find('.pagination-pager'),
			ellipsisStart: this.target.find('.pagination-ellipsis-start'),
			ellipsisEnd: this.target.find('.pagination-ellipsis-end'),
			block: this.target.find('.pagination-block'),
			next: this.target.find('[data-event="next"]'),
			previous: this.target.find('[data-event="previous"]')
		};

		this.prop.ellipsis['start'].el = this.sel.ellipsisStart;
		this.prop.ellipsis['end'].el = this.sel.ellipsisEnd;

		this.target
			.attr('tabindex', 0);
	}

	//
	// Setup
	//
	setup() {

	}

	//
	// Create the page objects
	//
	createPages() {
		let total = this.model.get('total');
		this.pages = [];

		for(var i = 0, il = total; i < il; ++i) {
			this.pages.push(new PaginationItem({
				controller: this,
				value: i + 1,
				total: total
			}));
		}
	}

	//
	// Render the component
	//
	render() {
		this.el = $( this.util.request(
			'template:render',
			Template,
			this.model.attributes
		));

		this.target
			.addClass('pagination')
			.empty()
			.append(this.el);
	}

	//
	// Bind the listeners
	//
	listen() {
		let keyboard = this.model.get('keyboard');

		this.model
			.on('change', this.onChanged, this);

		this.target
			.on('click', '[data-page]', this.onPageSelect.bind(this));

		this.sel.next
			.on('click', this.onNext.bind(this));

		this.sel.previous
			.on('click', this.onPrevious.bind(this));

		$(window)
			.on(`resize${EventKey}.${this.uid}`,
				this.util.request('events:throttle',
					this.refresh.bind(this),
					1
				)
			);

		// Keyboard controls
		if(keyboard) {
			this.target
				.on(`keydown${EventKey}`,
					this.onKeyDown.bind(this)
				)
				.on(`keyup${EventKey}`,
					this.onKeyUp.bind(this)
				)
		}

		this.refresh();
	}

	//
	// Unlisten (on destroy)
	//
	unlisten() {
		let keyboard = this.model.get('keyboard');

		this.model.off('change');
		this.target.off('click', '[data-page]');
		this.sel.next.off('click');
		this.sel.previous.off('click');

		if(keyboard) {
			this.target
				.off(`keydown${EventKey}`)
				.off(`keyup${EventKey}`);
		}

		window.off(`resize${EventKey}.${this.uid}`);
	}

	//
	// Changed
	//
	onChanged(e) {
		if(e.current != undefined) {
			this.changePage();
		}
	}

	//
	// Set the current value with clamp
	//
	setCurrent(curr) {
		if(curr != null)
			this.model.attributes.current = curr;

		let current = this.model.get('current'),
			total = this.model.get('total');

		current = clamp(1, total, current);

		this.model.attributes.current = current;
		this.refreshPager();

		return current;
	}

	//
	// On Page Select
	//
	onPageSelect(e) {
		let page = $(e.currentTarget).data('page');
		this.set('current', page);
	}

	//
	// On Next Page
	//
	onNext(e, time) {
		if(time == null)
			time = this.model.get('nextPrevAnimationDuration');

		this.setCurrent(this.model.get('current') + 1);
		this.changePage(time);

		if(e)
			e.preventDefault();
	}

	//
	// On Previous Page
	//
	onPrevious(e, time) {
		if(time == null)
			time = this.model.get('nextPrevAnimationDuration');

		this.setCurrent(this.model.get('current') - 1);
		this.changePage(time);

		if(e)
			e.preventDefault();
	}

	//
	// Set Initial value (which runs with no animation)
	//
	setInitial() {
		let current = this.setCurrent();
		this.tweenTo(current, 0);
	}


	//
	// Change the page
	//
	changePage(time) {
		let current = this.setCurrent();
		this.tweenTo(current, time);

		// Don't emit changes while suppressed (e.g. key is down)
		if(!this.prop.suppressChanges)
			this.emit('change', current);
	}

	//
	// On Key Down
	//
	onKeyDown(e) {
		let time = this.model.get('keyAnimationDuration');
		e.preventDefault();
		if(/input|textarea/i.test(e.target.tagName))
			return;

		// Suppress change events until key up to prevent rapid emitting
		this.prop.suppressChanges = true;

		switch(e.which) {
			case 9: this.onNext(null, time); break;
			case 37: this.onPrevious(null, time); break;
			case 39: this.onNext(null, time); break;
			default: return;
		}
	}

	//
	// On Key Up
	//
	onKeyUp(e) {
		// Re-enable change events
		this.prop.suppressChanges = false;
		this.emit('change', this.model.get('current'));
	}

	//
	// Refresh the pagination controls based on the current selection
	//
	refreshPager() {
		let current = this.model.get('current');
		if(current > 1)
			this.sel.previous
				.parent()
				.removeClass('pagination-pager-disabled');
		else
			this.sel.previous
				.parent()
				.addClass('pagination-pager-disabled');

		if(current <= this.model.get('total') - 1)
			this.sel.next
				.parent()
				.removeClass('pagination-pager-disabled');
		else
			this.sel.next
				.parent()
				.addClass('pagination-pager-disabled');
	}

	//
	// Tween to the value
	//
	tweenTo(value, time) {
		if(this.tween)
			this.tween.stop();

		this.tween = new Tween({
				start: this.prop.activeValue,
				end: value,
				duration: time != null ?
					time * 1000 :
					this.model.get('animationDuration') * 1000
			})
			.on('tick', val => {
				this.prop.activeValue = val;
				this.update();
			})
			.on('done', () => {
				this.update();
			})
			.begin();
	}

	//
	// Refresh
	//
	refresh() {
		this.update(true);
		//this.update();
	}

	//
	// Update the display
	//
	update(isResize) {
		if(!this.isVisible())
			return;

		let val = this.prop.activeValue;

		this.updateLayout();
		this.calculate();
		this.reflow();
	}

	//
	// Update the layout based on the current dimensions
	//
	updateLayout() {
		let breakpoint = this.model.get('breakpoint'),
			width = this.target.width();

		if(width <= breakpoint) {
			if(!this.prop.breakpoint) {

				this.target
					.addClass('pagination-reduced');

				this.prop.breakpoint = true;
			}
		} else if(this.prop.breakpoint) {

			this.target
				.removeClass('pagination-reduced');

			this.prop.breakpoint = false;
		}
	}


	//
	// Calculate the display
	//
	calculate() {
		let val = {};

		// Run through the various calculations
		this.calculateBase(val);
		this.calculateDimensions(val);
		this.calculateMiddleItem(val);
		this.calculateOffsets(val);
		this.calculateWidths(val);
		this.calculateListOffset(val);
		this.calculateBlock(val);
		this.calculateOverflow(val);

		// Set the parameters
		this.params = val;

		//console.log(this.params);

		// Set the width of the list
		this.sel.list
			.width(val.availableWidth - val.spacer);
	}

	//
	// Calculate the base settings
	//
	calculateBase(val) {
		this.sel.list.css({width: 'auto'});

		val.total = this.model.attributes.total;
		val.size = this.model.attributes.itemSize;
		val.spacer = this.model.attributes.itemSpacer;
		val.itemWidth = val.size + val.spacer;

		val.activeValue = this.prop.activeValue;
		val.width = this.sel.list.width();

		val.maxItems = Math.floor(
			(((val.width - (val.spacer * .5)) + val.spacer) / val.itemWidth)
		);
	}

	//
	// Calculate our dimensions to make sure we have a workable pagination display.
	//
	calculateDimensions(val) {
		if(val.maxItems < 7) {
			this.target.addClass('pagination-mini');
			val.spacer = Math.floor(Math.max(0, Math.floor(val.width) -
				(7 * val.size)) / 7);
			val.maxItems = 7;
		} else {
			this.target.removeClass('pagination-mini');
		}

		val.itemWidth = val.spacer + val.size;
	}

	//
	// Calculate the middle item
	//
	calculateMiddleItem(val) {
		val.middleItem = Math.floor(val.maxItems * .5);
	}

	//
	// Calculate the scroll offset (centerizing)
	//
	calculateOffsets(val) {
		let align = this.model.get('align'),
			centerThreshold = this.model.get('centerThreshold');

		val.numOffsetLeft = Math.max(0, Math.min(
			Math.max(
				val.middleItem, val.activeValue - 1) - (val.middleItem),
				val.total - val.maxItems
			)
		);

		val.numOffsetRight = Math.max(val.middleItem + 1, val.activeValue) +
			(val.maxItems - val.middleItem) - 2;

		let underflow = val.maxItems - val.middleItem + 1 >= val.total,
			centered = val.width > centerThreshold || underflow;

		if(centered && align === 'center') {

			let {centerOffsetLeft, centerOffsetRight} = this.model.attributes;

			if(underflow) {
				centerOffsetLeft = 0;
				centerOffsetRight = 0;
			}

			let left = Math.max(
					0,
					val.middleItem + 1 - centerOffsetLeft -
						val.numOffsetLeft - val.activeValue
				),
				right = Math.min(
					0,
					(val.numOffsetLeft + val.middleItem + 1 +
						centerOffsetRight) - val.activeValue
				);

			val.scrollOffset = (left + right) * val.itemWidth / 2;
			val.scrollOffsetPage = (left + right);
		} else {
			val.scrollOffset = 0;
			val.scrollOffsetPage = 0;
		}

		val.numOffsetLeft -= Math.min(0, val.scrollOffsetPage);
		val.numOffsetRight -= Math.max(0, val.scrollOffsetPage);
	}

	//
	// Calculate the widths
	//
	calculateWidths(val) {
		val.availableWidth = (val.maxItems * val.itemWidth);
		val.listWidth = (this.pages.length * val.itemWidth);
	}

	//
	// Calculate the page list offset
	//
	calculateListOffset(val) {
		// Calculate offset
		val.offset = -((val.activeValue - 1) * val.itemWidth);
		val.offset += (val.middleItem * val.itemWidth);
		val.offset = Math.max(-val.listWidth + val.availableWidth, Math.min(
			val.offset,
			0
		));
		val.offset = Math.min(0, val.offset);
	}

	//
	// Calculate the block that tracks the current page number
	//
	calculateBlock(val) {
		val.closestIndex = clamp(
			0,
			this.pages.length - 1,
			Math.round(val.activeValue) - 1
		);

		val.blockPos = (
			(
				(val.activeValue - Math.max(0, val.numOffsetLeft)) - 1) * val.itemWidth
			) + Math.max(0, val.scrollOffset);
	}

	//
	// Calculate overflow
	//
	calculateOverflow(val) {
		val.overflowStart = val.activeValue - .5 > (val.middleItem + 1) +
			(val.total <= val.maxItems ? 1 : 0);

		val.overflowEnd = val.activeValue + .5 < val.total -
			(val.maxItems - val.middleItem) + 1 -
			(val.total <= val.maxItems ? 1 : 0);

		if(val.total <= (val.maxItems - val.scrollOffsetPage)) {
			val.overflowEnd = false;
		}
	}

	//
	// Reflow the items
	//
	reflow() {
		let activeValue = this.prop.activeValue;

		this.positionBlock(this.params);
		this.positionEllipsis();

		for(let page of this.pages)
			page.position(this.params);
	}

	//
	// Position the block overlay
	//
	positionBlock(params) {
		let blockWidth = this.pages[params.closestIndex].getWidth();
		if(this.prop.block.pos !== params.blockPos) {
			this.prop.block.pos = params.blockPos;
			this.sel.block.css({
				transform: `translateX(${params.blockPos}px)`
			});
		}

		if(this.prop.block.width !== blockWidth) {
			if(this.prop.block.widthTween)
				this.prop.block.widthTween.stop();

			this.prop.block.width = blockWidth;

			this.prop.block.widthTween = new Tween({
					start: this.prop.block.activeWidth,
					end: this.prop.block.width,
					duration: this.model.get('animationDuration') * 500
				})
				.on('tick', val => {
					this.prop.block.activeWidth = val;
					this.sel.block.css({
						width: this.prop.block.activeWidth
					});
				})
				.begin();
		}
	}

	//
	// Position the ellipsis elements
	//
	positionEllipsis() {
		let start = this.params.itemWidth,
			end = this.params.scrollOffset + (this.params.maxItems - this.params.scrollOffsetPage - 2) * this.params.itemWidth;

		this.prop.ellipsis['start'].el.css({
			transform: `translateX(${start}px)`
		});

		this.prop.ellipsis['end'].el.css({
			transform: `translateX(${end}px)`
		});

		this.updateEllipsisVisibility('start', this.params.overflowStart);
		this.updateEllipsisVisibility('end', this.params.overflowEnd);
	}

	//
	// Update the visibility of the start/end ellipsis
	//
	updateEllipsisVisibility(type, show) {
		if(show)
			this.showEllipsis(type);
		else
			this.hideEllipsis(type);
	}

	//
	// Show ellipsis
	//
	showEllipsis(type) {
		let ellipsis = this.prop.ellipsis[type];

		if(ellipsis.shown)
			return;

		ellipsis.shown = true;
		ellipsis.el.addClass('active');
	}

	//
	// Hide ellipsis
	//
	hideEllipsis(type) {
		let ellipsis = this.prop.ellipsis[type];

		if(!ellipsis.shown)
			return;

		ellipsis.shown = false;
		ellipsis.el.removeClass('active');
	}

	//
	// Check if the pagination is visible
	//
	isVisible() {
		return Boolean(
			this.target[0]
				.offsetParent
		);
	}

	//
	// Interface
	//
	static _interface(opts) {
		let $el = $(this),
			data = $el.data(DataKey),
			config = Object.assign(
				{},
				$el.data(),
				opts
			);

		if(!data) {
			data = new PaginationView({
				target: $el,
				model: new PaginationModel(config)
			});

			$el.data(DataKey, data);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		this.unlisten();
		$.removeData(this.target, DataKey);
	}
}



//
// Pagination Item (Represents a single page element in the pagination display)
//
class PaginationItem extends Model {
	constructor (opts) {
		super(opts);

		this.prop = {
			fadeAnimationDuration: this.get('fadeAnimationDuration'),
			firstPosition: false,
			fadeTween: null,
			opacity: 1,
			created: false,
			inDOM: false,
			active: false,
			pos: {
				x: 0
			}
		};

		this.controller = this.get('controller');
	}

	//
	// Position the element
	//
	position(params) {
		let val = this.attributes.value - 1,
			// Is the page in the view boundaries
			inView = val > (params.numOffsetLeft) - 1 &&
				val < (params.numOffsetRight),
			// Fade out the page number if its left position is falling out
			// of the boundaries
			opacityLeft = Number(parseFloat(clamp(0, 1,
				this.attributes.value - params.numOffsetLeft -
					(params.overflowStart && val !== 1 ? 2 : 1)
			)).toFixed(2)),
			// Fade out the page number if its right position is falling out
			// of the boundaries
			opacityRight = Number(parseFloat(clamp(0, 1,
				-(this.attributes.value - params.numOffsetRight -
				(params.overflowEnd && val !== this.attributes.total - 2  ?
					0 :
					1)
				)
			)).toFixed(2)),
			// Calculate the final opacity based on the left/right values
			opacity = Math.min(opacityLeft, opacityRight);

		if(this.attributes.value === 1)
			return this.positionFirst(params);

		if(this.attributes.value === this.attributes.total)
			return this.positionLast(params);

		if(!inView)
			return this.remove();

		this.add();
		this.updateState(params);
		this.updateOpacity(opacity);

		this.prop.pos.x = params.offset + (val) *
			params.itemWidth + params.scrollOffset;

		this.el.css({
			transform: `translateX(${this.prop.pos.x}px)`
		});
	}

	//
	// Update the active state of the page
	//
	updateState(params) {
		if(Math.round(params.activeValue) === this.attributes.value) {

			if(!this.prop.active) {
				this.el
					.addClass('active')
					.find('a')
					.attr('aria-label', `Current Page (${this.attributes.value})`);

				this.prop.active = true;
			}

		} else if(this.prop.active) {
			this.el
				.removeClass('active')
				.find('a')
				.attr('aria-label', `Page ${this.attributes.value}`);

			this.prop.active = false;

		}
	}

	//
	// Update the opacity
	//
	updateOpacity(opacity) {
		if(this.prop.opacity === opacity)
			return;

		this.prop.opacity = opacity;
		this.el.css({
			opacity: this.prop.opacity
		});

	}

	//
	// Set first item
	//
	positionFirst(params) {
		this.add();
		this.updateState(params);

		if(this.prop.firstPosition === params.scrollOffset)
			return;

		this.prop.firstPosition = Math.max(0, params.scrollOffset);

		this.el.css({
			transform: `translateX(${this.prop.firstPosition}px)`
		});
	}

	//
	// Position the last item
	//
	positionLast(params) {
		this.add();
		this.updateState(params);

		let pos = params.scrollOffset + (params.maxItems - params.scrollOffsetPage - 1) * params.itemWidth +
			Math.min(0, params.scrollOffset),
			relPos = params.offset + (this.attributes.value - 1) *
				params.itemWidth + params.scrollOffset;

		pos = Math.min(pos, relPos);

		this.el.css({
			transform: `translateX(${pos}px)`
		});
	}

	//
	// Remove item from DOM if necessary
	//
	remove() {
		if(!this.prop.inDOM)
			return;

		this.prop.inDOM = false;
		this.el.remove();
	}

	//
	// Add item to DOM if necessary
	//
	add() {
		if(this.prop.inDOM)
			return;

		if(!this.created)
			this.render();

		this.prop.inDOM = true;
		this.controller.sel.list.append(this.el);
		this.el.css({
			opacity: this.prop.opacity
		});
	}

	//
	// Get the width of the page number
	//
	getWidth() {
		return this.prop.inDOM ? this.el.width() : 20;
	}

	//
	// Render
	//
	render() {
		var {value, total} = this.attributes;

		this.el = $( this.util.request(
			'template:render',
			TemplateItem,
			Object.assign(
				this.attributes,
				{
					key: value === 1 || value === total
				}
			)
		));

		this.created = true;
	}
}
