//
// Search Component
//

import View from '../../core/view';
import Component from '../classes/component';
import SearchModel from '../models/search.js';

//
// Configuration
//
const Name = 'Search';
const DataKey = 'dls.search';
const EventKey = `.${DataKey}`;

let inputIsDisabled = (input) => {
	return input.is(':disabled') || input.is('.disabled');
};

//
// Initiator
//
export default class Search extends Component {

	constructor (opts) {
		super(opts);
		this.component = SearchView;
	}
}

//
// Search View
//
class SearchView extends View {

	constructor (opts) {
		super(opts);

		this.setProps();
		this.createTooltip();
		this.listen();
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get EventKey() {return EventKey;}

	//
	// Store the properties
	//
	setProps() {
		this.sel = {
			input: this.target.find('input'),
			submit: this.target.find('button[type="submit"]')
		};

		this.model.attributes.value = this.sel.input.val();
		this.sel.submit.prop('type', 'button');
	}

	//
	// Create tooltip
	//
	createTooltip() {
		if(!this.model.get('tooltip'))
			return;

		this.tooltip = this.app.create("tooltip", {
			target: this.sel.submit,
			config: {
				content: this.getTooltipLabel(),
				theme: this.model.get('tooltipTheme'),
				delay: this.model.get('tooltipDelay'),
				size: 'sm',
				placement: this.model.get('tooltipPlacement'),
				classes: this.model.get('tooltipClasses'),
				style: this.model.get('tooltipStyle'),
				trigger: 'manual',
				offset: this.model.get('tooltipOffset')
			}
		});
	}

	//
	// Get the tooltip label text based on the current state
	//
	getTooltipLabel() {
		let state = this.model.get('state');
		state = state.charAt(0).toUpperCase() + state.slice(1);

		return this.model.get(`tooltip${state}`) || '';
	}

	//
	// Listen
	//
	listen() {

		if(this.model.get('live'))
			this.sel.input
				.on(this.model.get('keyevent'),
					this.util.request(
						'events:throttle',
						$.proxy(this.onChange, this),
						this.model.get('throttle'),
						this
					)
				);

		this.target
			.on('submit', $.proxy(this.onFormSubmit, this));

		this.sel.submit
			.on(this.platform.event.over, $.proxy(this.onSubmitOver, this))
			.on(this.platform.event.out, $.proxy(this.onSubmitOut, this))
			.on('click.submit', $.proxy(this.onSubmit, this));

		this.model
			.on('change', $.proxy(this.onChanged, this));
	}

	//
	// Submit hover over
	//
	onSubmitOver() {
		if(!this.tooltip)
			return;

		this.tooltip.set('content', this.getTooltipLabel());
		if(this.tooltip.get('content')!=='')
			this.tooltip.show();
	}

	//
	// Submit hover out
	//
	onSubmitOut() {
		if(!this.tooltip)
			return;

		this.tooltip.hide();
	}

	//
	// On Form Submit
	//
	onFormSubmit(e) {
		let state = this.model.get('state');
		e.preventDefault();

		if(inputIsDisabled(this.sel.input))
			return;

		if(state === 'searching')
			return;
		else if(state === 'default' || state === 'active')
			this.onChange(true);
	}

	//
	// Submit search in its current state
	//
	onSubmit(e) {
		let state = this.model.get('state');
		e.preventDefault();
		if(inputIsDisabled(this.sel.input))
			return;

		if(state === 'searching')
			return;
		else if(state === 'active')
			this.reset(true);
		else if(state === 'default')
			this.onChange(true);
	}

	//
	// Reset the search to its default state
	//
	reset(focus) {
		this.sel.input.val('');
		this.onChange();

		if(focus)
			this.sel.input.focus();
	}

	//
	// Key changed
	//
	onChange(manual) {
		let val = this.sel.input.val();

		if(this.model.get('value') === val && !manual)
			return;

		this.model.attributes.value = this.sel.input.val();

		this.emit('change', this.model.get('value'));
	}

	//
	// Model changed
	//
	onChanged(obj) {
		let value = this.model.get('value');

		if(obj.value !== undefined)
			this.sel.input.val(value);

		if(obj.state !== undefined)
			this.target.attr('data-state', obj.state);
	}


	//
	// Interface
	//
	static _interface(config) {
		let $el = $(this),
			data = $el.data(DataKey);

		config = Object.assign($el.data(), config);

		if(!data) {
			data = new SearchView({
				target: $el,
				model: new SearchModel(config),
				value: $el.find('input').val()
			});

			$el.data(DataKey, data);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
