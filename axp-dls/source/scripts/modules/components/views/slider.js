//
// Slider Component
//

import View from '../../core/view';
import Component from '../classes/component';
import SliderModel from '../models/slider.js';
import SliderTemplate from './templates/tmpl-slider.html';

//
// Configuration
//
const Name = 'Slider';
const DataKey = 'dls.slider';
const EventKey = `.${DataKey}`;
const Step = {
	amount: 0,
	timeout: null,
	interval: null
}

//
// Initiator
//
export default class Slider extends Component {

	constructor (opts) {
		super(opts);
		this.component = SliderView;
	}


}

//
// Slider View
//
class SliderView extends View {

	constructor (opts) {
		super(opts);

		if(!this.model.get('step'))
			this.model.set('step', 1);

		this.Step = {
			amount: 0,
			timeout: null,
			interval: null
		};

		this.range = Math.sqrt(
			Math.pow( this.model.get('min') - this.model.get('max') , 2 )
		);

		this.render();

		this.setProps();
		this.createDraggable();
		this.createTooltip();

		this.listen();
		this.setPositionToValue(this.model.get('value'), true);
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get EventKey() {return EventKey;}

	//
	// Render the component
	//
	render() {
		this.el = $( this.util.request(
			'template:render',
			SliderTemplate,
			this.model.attributes
		));

		this.target
			.hide()
			.after(this.el);
	}

	//
	// Set the properties
	//
	setProps() {
		this.sel = {
			bar: this.el.find('.slider-bar'),
			handle: this.el.find('.slider-handle'),
			track: this.el.find('.slider-track'),
			selection: this.el.find('.slider-selection'),
			add: this.el.find('.slider-add'),
			subtract: this.el.find('.slider-subtract')
		};
	}

	//
	// Create draggable control
	//
	createDraggable() {
		this.draggable = this.util.request('ui:draggable', {
			target: this.sel.handle
		});
	}

	//
	// Create the tooltip
	///
	createTooltip() {
		if(this.model.get('tooltip') !== true) {
			this.sel.handle.removeAttr('data-toggle');
			return;
		}

		this.tooltip = this.app.create('tooltip', {
			target: this.sel.handle,
			config: {}
		});
	}

	//
	// Bind the listeners
	//
	listen() {
		this.draggable
			.on('change', this.onDragState, this)
			.on('drag:start', this.onDragStart, this)
			.on('drag:end', this.onDragEnd, this);

		this.model
			.on('change', this.changed, this);

		this.sel.track
			.on(this.platform.event.down + EventKey,
				$.proxy(this.draggable.skipToPosition, this.draggable)
			);

		this.sel.add
			.on(this.platform.event.down + EventKey,
				$.proxy(this.onAdd, this)
			);

		this.sel.subtract
			.on(this.platform.event.down + EventKey,
				$.proxy(this.onSubtract, this)
			);
	}

	//
	// Drag state change
	//
	onDragState(e) {
		if(e.dragging) {
			let offsetLeft = this.sel.handle.offset().left,
				width = this.sel.handle.width();

			this.startPos = offsetLeft + (width * 0.5);
		}

		if(!this.draggable.attributes.dragging)
			return;

		this.updatePosition(e);
	}

	//
	// Drag Start
	//
	onDragStart(e) {
		if(this.tooltip)
			this.tooltip.show(true);
	}

	//
	// Drag end
	//
	onDragEnd(e) {
		if(this.tooltip)
			this.tooltip.hide(true);
	}

	//
	// On Add
	//
	onAdd(e) {
		if(e.which === 3)
			return;

		this.Step.amount = this.model.get('step');
		this.stepToggleOn();
	}

	//
	// On Subtract
	onSubtract(e) {
		if(e.which === 3)
			return;

		this.Step.amount = -this.model.get('step');
		this.stepToggleOn();
	}

	//
	// Step Toggle
	//
	stepToggleOn() {
		let {
			value,
			min,
			max,
			stepDelay,
			stepInterval
		} = this.model.attributes;

		clearTimeout(this.Step.timeout);
		clearTimeout(this.Step.interval);

		if(this.tooltip)
			this.tooltip.show(true);

		value = Math.max(min, Math.min(value + this.Step.amount, max));

		this.setPositionToValue(value);

		this.Step.timeout = setTimeout(() => {
			this.Step.interval = setInterval(() => {
				value = Math.max(min, Math.min(value + this.Step.amount, max));

				this.setPositionToValue(value);

				if(this.tooltip)
					this.tooltip.trigger('tether:position');

			}, stepInterval)
		}, stepDelay);

		if(this.tooltip)
			this.tooltip.trigger('tether:position');

		$(document)
			.on(this.platform.event.up + EventKey + this.uid,
				$.proxy(this.stepToggleOff, this)
			);
	}

	//
	// Step Toggle Off
	//
	stepToggleOff() {
		clearTimeout(this.Step.timeout);
		clearTimeout(this.Step.interval);

		if(this.tooltip)
			this.tooltip.delayedHide(1000);

		$(document)
			.off(this.platform.event.up + EventKey + this.uid,
				$.proxy(this.stepToggleOff, this)
			);
	}

	//
	// Update Position
	//
	updatePosition(e) {
		let min = this.model.get('min'),
			percentage = this.getPercentage(this.draggable.attributes.movedX),
			value,
			calc = (this.fixToStep(this.range / 100 * percentage, percentage));

		percentage = (calc / this.range) * 100;
		value = Number(((this.range / 100 * percentage) + min).toFixed(3));

		this.setPositionToValue(value);

		if(this.tooltip)
			this.tooltip.trigger('tether:position');
	}

	//
	// Fix to step
	//
	fixToStep(num, percentage) {
		let step = this.model.get('step'),
			resto = num % step,
			value;

		if(!percentage)
			value = 0;
		else if(percentage === 100)
			value = this.range;
		else if(resto <= (step / 2))
			value = num - resto;
		else
			value = num + step - resto;

		return value;
	}

	//
	// Set position to the calculated value
	//
	setPositionToValue(value, force) {
		if(this.model.get('value') !== value || force)
			this.model.set('value', value);
	}

	//
	// Get Percentage
	//
	getPercentage(pos) {
		let offset = this.sel.bar.offset().left,
			width = this.sel.bar.width(),
			distance = (this.startPos + pos) - offset,
			percentage = (distance / width) * 100;

		return Math.max(0, Math.min(100, percentage));
	}

	//
	// Changed
	//
	changed() {
		let value = this.model.get('value'),
			min = this.model.get('min'),
			percentage = (Math.abs(value - min) / this.range) * 100;


		this.sel.handle.attr('aria-valuenow', value);
		this.target.val(value);

		this.updateTooltipValue(value);

		this.sel.handle
			.css({left: `${percentage}%`});

		this.sel.selection
			.css({width: `${percentage}%`});

		this.emit('change', value);
	}

	//
	// Update the tooltip value
	//
	updateTooltipValue(value) {
		let prefix = this.model.get('prefix'),
			suffix = this.model.get('suffix'),
			formatted = this.util.request('format:numbers:commas', value),
			content = prefix + formatted + suffix;

		if(this.tooltip){
			this.tooltip.set('content', content);
			this.tooltip.trigger('tether:position');
		}
	}


	//
	// Interface
	//
	static _interface(opts) {
		let $el = $(this),
			data = $el.data(DataKey),
			config = Object.assign(
				{
					min: Number($el.prop('min')),
					max: Number($el.prop('max')),
					value: Number($el.prop('value')),
					step: Number($el.prop('step'))
				},
				$el.data(),
				opts
			);

		if(!data) {
			data = new SliderView({
				target: $el,
				model: new SliderModel(config)
			});

			$el.data(DataKey, data);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
