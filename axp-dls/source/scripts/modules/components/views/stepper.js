//
// Stepper Component
//

import View from '../../core/view';
import Component from '../classes/component';
import StepperModel from '../models/stepper.js';
import StepperTemplate from './templates/tmpl-stepper.html';

//
// Configuration
//
const Name = 'Stepper';
const DataKey = 'dls.stepper';
const EventKey = `.${DataKey}`;
const Step = {
	amount: 0,
	timeout: null,
	interval: null
}

//
// Initiator
//
export default class Stepper extends Component {

	constructor (opts) {
		super(opts);
		this.component = StepperView;
	}

}

//
// Stepper View
//
class StepperView extends View {

	constructor (opts) {
		super(opts);

		if(!this.model.get('step'))
			this.model.set('step', 1);

		this.render();

		this.setProps();

		this.listen();
		this.setValue(this.model.get('value'), true);
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get EventKey() {return EventKey;}

	//
	// Render the component
	//
	render() {
		this.el = $( this.util.request(
			'template:render',
			StepperTemplate,
			this.model.attributes
		));

		this.target
			.hide()
			.after(this.el);
	}

	//
	// Set the properties
	//
	setProps() {
		this.sel = {
			value: this.el.find('.stepper-value'),
			add: this.el.find('.stepper-add'),
			subtract: this.el.find('.stepper-subtract')
		};
	}

	//
	// Bind the listeners
	//
	listen() {
		this.model
			.on('change', this.changed, this);

		this.sel.add
			.on(this.platform.event.down + EventKey,
				$.proxy(this.onAdd, this)
			);

		this.sel.subtract
			.on(this.platform.event.down + EventKey,
				$.proxy(this.onSubtract, this)
			);
	}

	//
	// On Add
	//
	onAdd(e) {
		if(e.which === 3)
			return;

		Step.amount = this.model.get('step');
		this.stepToggleOn();
	}

	//
	// On Subtract
	onSubtract(e) {
		if(e.which === 3)
			return;

		Step.amount = -this.model.get('step');
		this.stepToggleOn();
	}

	//
	// Step Toggle On
	//
	stepToggleOn() {
		let {
			value,
			min,
			max,
			stepDelay,
			stepInterval
		} = this.model.attributes;

		clearTimeout(Step.timeout);
		clearTimeout(Step.interval);

		value = Math.max(min, Math.min(value + Step.amount, max));

		this.setValue(value);

		Step.timeout = setTimeout(() => {
			Step.interval = setInterval(() => {
				value = Math.max(min, Math.min(value + Step.amount, max));

				this.setValue(value);

			}, stepInterval)
		}, stepDelay);

		$(document)
			.on(this.platform.event.up + EventKey + this.uid,
				$.proxy(this.stepToggleOff, this)
			);
	}

	//
	// Step Toggle Off
	//
	stepToggleOff() {
		clearTimeout(Step.timeout);
		clearTimeout(Step.interval);

		$(document)
			.off(this.platform.event.up + EventKey + this.uid,
				$.proxy(this.stepToggleOff, this)
			);
	}

	//
	// Set the value
	//
	setValue(value, force) {
		if(this.model.get('value') !== value || force)
			this.model.set('value', value);
	}

	//
	// Changed
	//
	changed() {
		let value = this.model.get('value');

		this.target.val(value);

		this.sel.value.val(value);

		this.emit('change', value);
	}

	//
	// Interface
	//
	static _interface(opts) {
		let $el = $(this),
			data = $el.data(DataKey),
			config = Object.assign(
				{
					min: Number($el.prop('min')),
					max: Number($el.prop('max')),
					value: Number($el.prop('value')),
					step: Number($el.prop('step'))
				},
				$el.data(),
				opts
			);

		if(!data) {
			data = new StepperView({
				target: $el,
				model: new StepperModel(config)
			});

			$el.data(DataKey, data);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
