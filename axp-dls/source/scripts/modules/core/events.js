//
// Events Class
//
// Event Publisher/Subscriber handling

export default class Events {
	constructor () {
		this._callbacks = {};
		this._queue = {};
	}

	trigger (...args) {
		return this.emit(...args);
	}

	emit (event) {
		var args = [].slice.call(arguments, 1),
			callbacks = this._callbacks['$' + event];

		if (callbacks) {
			callbacks = callbacks.slice(0);

			for (var i = 0, len = callbacks.length; i < len; ++i) {
				if(callbacks[i].callback!==undefined) {
					callbacks[i].callback.apply(callbacks[i].context, args);
				}else {console.warn(`[DLS > Events] Undefined Event callback for event: ${event} `);}
			}
		}
		else {
			this._queue['$' + event] = arguments;
		}

		return this;
	}

	refireQueue (event) {
		if (this._queue['$' + event]) {
			var _queuedEvent = Object.create(this._queue['$' + event]);

			delete this._queue['$' + event];
			this.emit(_queuedEvent[0], _queuedEvent[1])
		}
	}

	on (eventname, callback, context) {
		if (!this._callbacks['$' + eventname])
			this._callbacks['$' + eventname] = [];

		this._callbacks['$' + eventname].push({callback: callback, context: context});
		this.refireQueue(eventname);

		return this;
	}

	once (eventname, callback, context){
		let on = function () {
			this.off(eventname, on);
			calback.apply(context, arguments);
		}

		on.fn = callback;
		this.on(eventname, on);
		return this;
	}

	off (event, fn) {
		this._callbacks = this._callbacks || {};

		// all
		if (0 == arguments.length) {
			this._callbacks = {};
			return this;
		}

		// specific event
		var callbacks = this._callbacks['$' + event];
		if (!callbacks)
			return this;

		// remove all handlers
		if (1 == arguments.length) {
			delete this._callbacks['$' + event];
			return this;
		}

		// remove specific handler
		var cb;
		for (var i = 0; i < callbacks.length; i++) {
			cb = callbacks[i];
			if (cb.callback === fn || cb.fn === fn) {
			  callbacks.splice(i, 1);
			  break;
			}
		}
		return this;
	}

	addEventListener (event, fn) {
		this.on(event, fn);
	}

	listeners (event) {
		return this._callbacks['$' + event] || [];
	}

	hasListeners (event) {
		return !! this.listeners(event).length;
	}

	removeListener (event, fn) {
		this.off(event, fn)
	}

	removeAllListeners (event, fn) {
		this.off()
	}

	removeEventListener (event, fn) {
		this.off(event, fn)
	}
}
