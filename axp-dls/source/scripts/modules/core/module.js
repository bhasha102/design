//
// Module
//

'use strict';

import Channel from './radio';

export default class Module extends Channel {

	constructor (opts) {
		super(opts);
		this._applyConfig(opts);
		this.initialize();
		this._createSubmodules();
		if(this.get('autoStart'))
			this.start();
	}

	//
	// Initialize
	//
	initialize() {}

	//
	// Start
	//
	start() {
		this.onStart();
		this._startSubmodules();
	}

	//
	// On Start
	//
	onStart() {

	}

	//
	// Apply the Config if exists
	//
	_applyConfig(opts) {
		if(typeof opts === 'object')
			Object.assign(this, opts);

		if(this.constructor.prototype.Config)
			Object.keys(this.constructor.prototype.Config).forEach((key) => {
				this.set(key, this.constructor.prototype.Config[key]);
			});
	}

	//
	// Create Submodules
	//
	_createSubmodules() {
		let submodules = this.get('submodules'),
			submoduleOptions = this.get('submoduleOptions');
		Object.keys(submodules).forEach((key) => {
			this[key] = this.attributes.submodules[key] = new submodules[key](Object.assign({},
				{
					parent: this
				},
				submoduleOptions
			));
		});
	}

	//
	// Start the submodules
	//
	_startSubmodules() {
		let submodules = this.get('submodules');
		Object.keys(submodules).forEach((key) => {
			if(submodules[key].get('startWithParent'))
				submodules[key].start();
		});
	}
}

Module.prototype.defaults = {
	autoStart: false,
	startWithParent: true,
	submodules: {},
	submoduleOptions: {}
};
