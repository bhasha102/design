
import Events from './events';

export default class Obj {
	constructor (opts) {
		this.events = this.emitter();
		if (opts)
			this.extend(opts);
	}

	merge (obj1, obj2) {
		var obj3 = {};

		for (var attrname in obj1)
			obj3[attrname] = obj1[attrname];

		for (var attrname in obj2)
			obj3[attrname] = obj2[attrname];

		return obj3;
	}

	extend (obj) {
		if(obj && typeof obj === 'object')
			Object.keys(obj)
				.forEach(opt => {
					this[opt] = obj[opt];
				});

		return this;
	}

	emitter (obj) {
		return new Events(obj || this);
	}

	emit (eventname, obj) {
		this.events.emit(eventname, obj);
		return this;
	}

	trigger (eventname, obj) {
		this.emit(eventname, obj);
		return this;
	}

	on (eventname, callback, context) {
		return this.events.on(eventname, callback, context);
	}

	off (eventname, callback, context) {
		return this.events.off(eventname, callback, context);
	}
}
