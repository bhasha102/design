//
// Tweening
//

import Events from './events';

const Defaults = {
	duration: 1000,
	ease: (t, b, c, d) => {
		if ((t /= d / 2) < 1) return c / 2 * t * t + b
		return -c / 2 * ((--t) * (t - 2) - 1) + b
	}
};

export default class Tween extends Events {

	constructor (opts = {}) {
		super(opts);
		Object.assign(this, {}, Defaults, opts);

		this.frame = null;
		this.next = null;
		this.isRunning = false;
		this.events = {};
		this.direction = this.start < this.end ? 'up' : 'down';
	}

	//
	// Begin
	//
	begin() {
		if(!this.isRunning && this.next !== this.end)
			this.frame = requestAnimationFrame(this._tick.bind(this));
		return this;
	}

	//
	// Stop
	//
	stop() {
		cancelAnimationFrame(this.frame);
		this.isRunning = false;
		this.frame = this.timeStart = this.next = null;

		return this;
	}

	//
	// Tick
	//
	_tick(currentTime) {
		this.isRunning = true;

		let lastTick = this.next || this.start;

		if(!this.timeStart)
			this.timeStart = currentTime;

		this.timeElapsed = currentTime -  this.timeStart;
		this.next = (this.ease(
			this.timeElapsed,
			this.start,
			this.end - this.start,
			this.duration
		));

		if(this._shouldTick(lastTick)) {
			this.emit('tick', this.next);
			this.frame = requestAnimationFrame(this._tick.bind(this));
		} else {
			this.emit('tick', this.end);
			this.emit('done', null);
		}
	}

	//
	// Should tick
	//
	_shouldTick(lastTick) {
		return {
			up: this.next < this.end && lastTick <= this.next,
			down: this.next > this.end && lastTick >= this.next
		}[this.direction];
	}
}
