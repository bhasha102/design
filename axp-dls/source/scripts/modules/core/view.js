//
// View
//

import Obj from './obj';
import Model from './model';

export default class View extends Obj {

	constructor (opts) {
		super(opts);

		this.uid = this.util.request('uid:gen');

		if(!this.target)
			console.warn('[DLS > View]: No target specified for view');
		if(!this.model)
			this.model = new Model(opts);

		/*this.model.on('change', (...args) => {
			this.emit('change', args);
		});*/
	}

	render() {
		console.warn('[DLS > View]: No render available.')
	}

	//
	// Pass the 'get' to the model
	//
	get(...args) {
		return this.model.get(...args);
	}

	//
	// Pass the 'set' to the model
	//
	set(...args) {
		return this.model.set(...args);
	}
}
