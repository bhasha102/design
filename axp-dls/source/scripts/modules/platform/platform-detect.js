//
// Platform Detection Handling
//

import Obj from '../core/obj';

export default class PlatformDetect extends Obj {

	constructor (opts) {
		super(opts);
		this.detect();
		this.unify();
	}

	detect() {
		this.detectInput();
	}

	detectInput() {
		this.touch = (('ontouchstart' in window)
			|| (navigator.MaxTouchPoints > 0)
			|| (navigator.msMaxTouchPoints > 0));

	}

	unify() {
		this.unifyEvents();
	}

	unifyEvents() {
		if(this.touch) {
			this.event = {
				down: 'touchstart',
				up: 'touchend',
				over: 'touchstart',
				out: 'touchend',
				click: 'touchstart',
				move: 'touchmove',
				cancel: 'touchcancel',
				enter: 'touchstart',
				leave: 'toucheend'
			}
		} else {
			this.event = {
				down: 'mousedown',
				up: 'mouseup',
				over: 'mouseover',
				out: 'mouseout',
				click: 'mousedown',
				move: 'mousemove',
				cancel: 'mouseup',
				enter: 'mouseenter',
				leave: 'mouseleave',
			}
		}
	}
}
