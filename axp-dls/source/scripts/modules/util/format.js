//
// Formatting Utilities
//

import Obj from '../core/obj';

export default class Format extends Obj {

	constructor (opts) {
		super(opts);
		this.listen();
	}

	//
	// Listen
	//
	listen() {
		this.util
			.reply('format:numbers:commas', this.numberWithCommas, this)
			.reply('format:selector:autoprefix', this.autoPrefixSelector, this);
	}

	//
	// Number with commas
	//
	// Applies commas to a number (e.g. - 10000 > 10,000)
	numberWithCommas(x) {
		let parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
	}

	//
	// Auto prefix supplied selectors where a type has not been specified.
	// e.g. -
	//	'class-name' => '.class-name'
	//	'.class-name' => '.class-name'
	//	'#class-name' => '#class-name'
	//
	autoPrefixSelector(selector) {
		let prefix = selector.substr(0,1);
		return prefix !== '.' && prefix !== '#' ? `.${selector}` : selector;
	}
}
