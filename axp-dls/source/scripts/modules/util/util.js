//
// Utility Functions
//

import Obj from '../core/obj';
import Channel from '../core/radio';
import Format from './format';
import Debugger from './debugger';
import Templates from './templates';
import Draggable from './draggable';

let uid = 0;

export default class Util extends Channel {

	constructor (opts) {
		super(opts);
		this.listen();
		this.setup();
	}

	//
	//	Setup the utilities
	//
	setup() {
		Object.assign(this, {
			debugger: new Debugger({util: this}),
			template: new Templates({util: this}),
			format: new Format({util: this}),
			draggable: Draggable
		});
	}

	listen() {
		this.reply('uid:gen', this.getUID, this)
			.reply('ui:draggable', this.createDraggable, this)
			.reply('events:throttle', this.throttle, this);
	}

	//
	// Get unique ID
	//
	getUID(prefix) {
		uid++;
		return prefix?prefix+uid:'el'+uid;
	}

	//
	// Create and return a draggable component with the supplied opts
	//
	createDraggable(opts) {
		return new this.draggable(opts);
	}

	//
	// Throttle
	//
	throttle(func, ms = 500, context = window) {
		let to,
			pass,
			wait = false;

		return (...args) => {
			let later = () => {
				func.apply(context, args);
			};

			if(!wait) {
				wait = true;
				clearTimeout(pass);
				to = setTimeout(() => {
					wait = false;
					later();
				}, ms);
			} else {
				clearTimeout(pass);
				pass = setTimeout(() => {
					later();
				}, ms);
			}
		}
	}
}
